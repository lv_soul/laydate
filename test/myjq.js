//立即执行函数，形成独立作用域
(function(global,factory){
    //判断是否为commonjs运行环境，选择向外暴露方式
    if(typeof module === "object" && typeof module.exports !== "object"){
        //TODO向外暴露jquery
    }else{
        factory(global);
    }
})(typeof window !== "undefined" ? window : this , function(window, noGlobal){
    /*常量定义区*/
    var version = "1.11.3";
    var class2type = {};
    //获取js对象中的toString方法区分对象
    var toString = class2type.toString;
    //获取js对象的hasOwnProperty方法,该方法判断本身是否包含某个属性
    var hasOwn = class2type.hasOwnProperty;
    //定义一个空对象
    var support = {};
    var document = window.document;
    //正则表达式匹配html或者#id的字符串,？：必须用在左括号后面，代表这个括号内容匹配但不保存在结果集中,[^任意内容]代表不匹配^后面的内容
    var rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    //正则表达式匹配单标签如<br/>或者没有属性或者没有内容的双标签<div></div>
    var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);
    //h5特有标签
    var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
		"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video";
    //判断是否包含html标签的正则表达式
    var rhtml = /<|&#?\w+;/;
	//获取标签名的正则表达式
    var rtagName = /<([\w:]+)/;
    //必须关闭这些标签去支持XHTML,XHTML必须严格的关闭和嵌套
    var wrapMap = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
		legend: [ 1, "<fieldset>", "</fieldset>" ],
		area: [ 1, "<map>", "</map>" ],
		param: [ 1, "<object>", "</object>" ],
		thead: [ 1, "<table>", "</table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        //在IE6-8中不能序列化link,script,style,或者任何h5标签,除非包含在前面没有中断字符的div中
        _default: support.htmlSerialize ? [0, "", ""] : [1, "X<div>","</div>"]
    };


    /*构造方法定义区*/
    var jQuery = function(selector, context){
        //jQuery构造函数实际返回的是原型对象上的init构造函数的实例
        return new jQuery.fn.init(selector, context);
    }

    //将原型对象赋值给jQuery fn简化原型对象的书写
    jQuery.fn = jQuery.prototype = {
        jquery : version,
        constructor : jQuery,

    }

    //定义extend方法,将扩展对象的值替换被扩展对象的同名属性值,如果扩展对象为数组或者对象并且设置为深度扩展则递归扩展
    jQuery.extend = jQuery.fn.extend = function(){
        //options:每次遍历的argument;src:每次遍历argument属性名与之对应的target属性名的值;name:每次遍历argument的属性名
        var options, src, name, copy,clone,
            //扩展对象是否为数组的标识位
            copyIsArray,
            //函数参数的长度
            length = arguments.length,
            //假设第一个参数为需要扩展的对象，如果为false的值则默认为空对象
            target = arguments[0] || {},
            //默认为不是深度复制
            deep = false,
            i = 1;
        //如果第一个参数为boolean值，则代表传递了深度复制的值,再将第二个参数作为需要扩展的对象,下标向后移动一位
        if(typeof target === "boolean"){
            deep = target;
            target = arguments[i] || {};
            i++;
        }
        //如果需要扩展的对象不是object对象或者函数,则将扩展的对象置为空对象
        if(typeof target !== "object" || !jQuery.isFunction(target)){
            target = {};
        }
        //如果参数的长度等于i值则意味着只传递了一个对象，则将当前jQuery对象作为需要扩展的对象，target作为扩展对象
        if(length === i){
            target = this;
            i--;
        }
        //遍历扩展对象
        for( ; i < length; i++){
            //将扩展对象赋值给options变量并且不能为空
            if((options = arguments[i]) != null){
                //遍历扩展对象的属性
                for(name in options){
                    //src代表被扩展对象对应的属性值
                    src = target[name];
                    //copy代表扩展对象对应的属性值
                    copy = options[name];
                    //如果扩展对象的属性值和被扩展对象属性值相同则继续下一个循环,防止递归。
                    if(src === copy){
                        continue;
                    }
                    //如果是深拷贝，并且扩展对象对应的属性值有值，并且扩展对象对应的属性值是数组或者纯粹的对象，则递归拷贝
                    if(deep && copy && ((copyIsArray = jQuery.isArray(copy)) || jQuery.isPlainObject(obj))){
                        if(copyIsArray){
                            copyIsArray = false;
                            //如果src不是数组，则将clone设置为空数组
                            clone = src && jQuery.isArray(src) ? src : [];
                        }else{
                            //如果src不是对象，则将clone设置为空对象
                            clone = src && jQuery.isPlainObject(src) ? src : {};
                        }
                        //因为extend返回值为被修改过的对象，所以可以直接赋值给target
                        target[name] = jQuery.extend(deep, clone, copy);
                    }else if(copy !== undefined){
                        target[name] = copy;
                    }
                }
            }
        }
        return target;
    }

    //通过extend为jQuery扩展功能
    jQuery.extend({
        //判断是否为window对象
        isWindow : function(obj){
            return obj != null && obj == obj.window;
        },

        //判断是否为函数
        isFunction: function(obj){
            return jQuery.type(obj) === "function";
        },

        //返回对象类型
        type : function(obj){
            //如果obj为null则返回"null"字符串
            if(obj == null){
                return obj + "";
            }
            //如果obj类型为对象或者函数则返回对应的对象类型否则返回对应typeof类型。toString方法返回的是包含对象类型的[object xxx]字符串。
            return typeof obj === "object" || typeof obj === "function" ? 
                class2type[toString.call(obj)] || "object" : 
                typeof obj;
        },

        //判断是否为数组
        isArray: Array.isArray || function(obj){
            return jQuery.type(obj) === "array";
        },

        //遍历方法,obj为要遍历的对象,callback为每次遍历需要回调的函数,args为每次回调的参数
        each : function(obj, callback, args){
            var value,
                i = 0,
                //length为遍历对象的长度
                length = obj.length,
                //判断遍历的对象是否为数组或者伪数组对象
                isArray = isArraylike(obj);
            //如果传递了args参数
            if(args){
                //遍历的对象是数组或者伪数组对象
                if(isArray){
                    //遍历数组或者伪数组对象
                    for( ; i < length; i++){
                        //每次利用函数的apply方法,调用函数。将每次遍历道对象作为函数的this对象和args作为参数传递给函数
                        value = callback.apply(obj[i], args);
                        //如果哪一次遍历时回调函数执行后返回false则跳出循环。
                        if(value === false){
                            break;
                        }
                    }
                }else{
                    for( i in obj){
                        value = callback.apply(obj[i], args);
                        if(value === false){
                            break;
                        }
                    }
                }
            }else{
                if(isArray){
                    for(; i < length; i++){
                        value = callback.apply(obj[i], i, obj[i]);
                        if(value === false){
                            break;
                        }
                    }
                }else{
                    for( i in obj){
                        value = callback.apply(obj[i], i, obj[i]);
                        if(value === false){
                            break;
                        }
                    }
                }
            }
            //返回遍历的对象
            return obj;
        },

        //判断是否为纯粹的对象，{}对象字面量形式或者new Object()形式创建出来的对象为纯净对象
        isPlainObject: function(obj){
            var key;
            //如果为null或者undefined或者类型不是object或者有nodeType属性或者是窗口对象则返回false
            if(!obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)){
                return false;
            }
            try{
                //如果对象有构造函数，且构造函数不在自身上，并且构造函数的原型上没有new Object产生对象的改造函数原型特有属性isPrototypeOf则认为不是纯粹对象
                if(obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")){
                    return false;
                }
            //IE89在执行上述操作时，会报错
            }catch(e){
                return false;
            }
            if(support.ownLast){
                for(key in obj){
                    return hasOwn.call(obj, key);
                }
            }
            //如果最后一个属性都为自身属性则该对象为纯粹对象
            for(key in obj){}
            return key == undefined || hasOwn.call(obj, key);
        },
        //合并数组,将第二个数组合并到第一个数组。如果第二个参数不是数组则返回第一个数组
        merge: function(first, second){
            var len = +second.length,
                j = 0,
                i = first.length;
            while(j < len){
                first[i++] = second[j++];
            }
            if(len !== len){
                while(second[j] !== undefined){
                    first[i++] = second[j++];
                }
            }
            first.length = i;
            return first;
        },
        //构建js片段TODO3 elems:字符串数组,context:上下文
        buildFragment: function(elems, context, scripts, selection){
            //nodes返回的节点数组,l代表字符串数组长度,safe代表创建的一个安全的文档片段,wrap代表正确闭合标签的嵌套标签
            var elem, tmp, tag, wrap, nodes = [], l = elems.length, safe = createSafeFragment(context),i = 0;
            for(; i < l; i++){
                elem = elems[i];
                if(elem || elem === 0){
                    //如果elem是对象,则直接添加nodes
                    if(jQuery.type(elem) === "object"){
                        //如果节点元素则添加到nodes中，否则不添加
                        jQuery.merge(nodes,elem.nodeType ? [elem] : elem);
                    }
                    //如果elem文本没有包含html标签则直接创建文本元素
                    else if(!rhtml.test(elem)){
                        nodes.push(context.createTextNode(elem));
                    }
                    //将文本转化为node节点
                    else{
                        tmp = tmp || safe.appendChild(context.createElement("div"));
                        //获取ele中的标签名,并转换为小写
                        tag = (rtagName.exec(elem) || ["",""])[1].toLowerCase();
                        //获取该标签对应的外层标签
                        wrap = wrapMap.tag || wrapMap._default;
                        //TODO
                    }
                }
            }
        }
    });

    //定义原型初始化方法，参数selector和context有12个分支
    var init = jQuery.fn.init = function(selector, context){
        var match;
        //分支1:selector可以转换为false，返回一个空的jQuery对象。如:$(""), $(null), $(undefined)
        if(!selector){
            return this;
        }
        //分支2:selector为字符串的参数
        if(typeof selector === "string"){
            //如果选择器左尖括号开始和右尖括号结束且字符数大于3个字符则默认为html标签字符串，跳过正则表达式的验证
            if(selector.charAt(0) === "<" && selector.charAt(selector.length - 1) === ">" && selector.length >= 3){
                match = [null, selector, null];
            }else{
                //如果匹配表达式则返回匹配值，否则返回null
                match = rquickExpr.exec(selector);
            }
            //如果通过正则表达式的匹配,且为html代码字符串或则为id选择器字符串并且没有传递context上下文
            if(match && (match[1] || !context)){
                //处理seletor为html字符串
                if(match[1]){
                    //修正context
                    context = context instanceof jQuery ? context[0] : context;
                    //将html字符串转换为dom
                    jQuery.merge(this,jQuery.parseHTML(
                        match[1], 
                        context && context.nodeType ? context.ownerDocument || context : document, 
                        true
                        ));
                }else{

                }
            }
        }
    }

    /*方法定义区*/
    //创建一个安全的代码片段
    function createSafeFragment(document){
        var list = nodeNames.split("|"),sageFragment = document.createDocumentFragment();
        if(sageFragment.createElement){
            while(list.length){
                //教会低版本浏览器创建h5元素
                sageFragment.createElement(list.pop());
            }
        }
        return sageFragment;
    }
    /*对象定义区*/

    /*其他区域*/
    //初始化类型对象
    jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(i, name){
        //构造与js对象的toString方法返回值对应的对象。
        class2type[ "[object "+ name +"]" ] = name.toLowerCase();
    });

    //将html字符串解析为dom
    jQuery.parseHTML = function(data, context, keepScripts){
            //如果为null或者data不是字符串则返回空
            if(!data || typeof data !== "string"){
                return null;
            }
            //如果context为布尔值，则代表context为用户传递的keepScripts的值
            if(typeof context === "boolean"){
                keepScripts = context;
                context = false;
            }
            //修正context
            context = context || document;
            //匹配字符串是否为单标签
            var parsed = rsingleTag.exec(data),
                scripts = !keepScripts && [];
            //如果是单标签则直接创建元素
            if(parsed){
                retrun [context.createElement(parsed[1])];
            }
            //如果不是片段则构建片段TODO2
            paered = jQuery.buildFragment([data], context, scripts)；

            
    };

    //判断是不是伪数组对象
    function isArraylike(obj){
        //如果obj有length属性则将length赋值给length变量
        var length = "length" in obj && obj.length,
            //判断对象的类型
            type = jQuery.type(obj);
        //如果类型是函数或者窗口对象则不是伪数组对象
        if(type === "funtion" || jQuery.isWindow(obj)){
            return false;
        }
        //如果是element对象并且length有值则认为是伪数组对象
        if(obj.nodeType === 1 && length){
            return true;
        }
        //如果类型为数组或者长度为0为伪数组如果length类型为number并且length大于0并且length - 1为obj的属性则认为是伪数组
        return type === "array" || length === 0 || 
            typeof length === "number" && length > 0 && (length - 1) in obj;
    }
    //如果是浏览器器环境则向外暴露jQuery
    var strundefined = typeof undefined;
    //如果能传递undefined则认为是浏览器模式
    if(typeof noGlobal === strundefined){
        //将$和jQuery暴露给window对象
        window.$ = window.jQuery = jQuery;
    }
});
